using UnityEngine;
using Unity.Netcode;

public class PlayerSpawnManager : NetworkBehaviour
{
    private bool hasSpawned = false;

    public override void OnNetworkSpawn()
    {
        if (!hasSpawned && IsOwner)
        {
            DontDestroyOnLoad(gameObject);
            if (IsServer)
            {
                NetworkManager.Singleton.OnClientConnectedCallback += OnClientConnected;
            }
            hasSpawned = true;
        }
    }

    private void OnDestroy()
    {
        if (IsOwner && IsServer)
        {
            NetworkManager.Singleton.OnClientConnectedCallback -= OnClientConnected;
        }
    }

    private void OnClientConnected(ulong clientId)
    {
        if (NetworkManager.Singleton.ConnectedClientsList.Count == 2)
        {
            MoveToSpawnPoint();
        }
    }

    private void MoveToSpawnPoint()
    {
        string spawnTag = IsHost ? "hostSpawn" : "clientSpawn";
        GameObject spawnPoint = GameObject.FindGameObjectWithTag(spawnTag);
        if (spawnPoint != null)
        {
            transform.position = spawnPoint.transform.position;
            transform.rotation = spawnPoint.transform.rotation;
        }
        else
        {
            Debug.LogError($"Spawn point with tag {spawnTag} not found!");
        }
    }
}
