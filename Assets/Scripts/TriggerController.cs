using UnityEngine;
using Unity.Netcode;

public class TriggerController : NetworkBehaviour
{
    public GameObject wall;
    public Vector3 targetPosition;
    private Vector3 initialPosition;

    void Start()
    {
        initialPosition = wall.transform.position;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && IsServer)
        {
            MoveWallClientRpc(targetPosition);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player") && IsServer)
        {
            MoveWallClientRpc(initialPosition);
        }
    }

    [ClientRpc]
    private void MoveWallClientRpc(Vector3 newPosition)
    {
        wall.transform.position = newPosition;
    }
}
