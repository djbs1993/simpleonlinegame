// LevelCompletion.cs
using UnityEngine;
using UnityEngine.SceneManagement;
using Unity.Netcode;

public class LevelCompletion : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (IsFinalLevel())
            {
                SceneManager.LoadScene("Congratulations");
            }
            else
            {
                LoadNextLevel();
            }
        }
    }

    private bool IsFinalLevel()
    {
        return SceneManager.GetActiveScene().name == "Level3";
    }

    private void LoadNextLevel()
    {
        string nextLevelName = "Level" + (int.Parse(SceneManager.GetActiveScene().name.Replace("Level", "")) + 1);
        SceneManager.LoadScene(nextLevelName);
    }
}
