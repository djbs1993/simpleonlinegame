// CongratulationsManager.cs
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI; // Add this line to use the Button class

public class CongratulationsManager : MonoBehaviour
{
    public Button returnButton; // Ensure you have the correct namespace for Button

    void Start()
    {
        returnButton.onClick.AddListener(ReturnToMainMenu);
    }

    public void ReturnToMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
