using UnityEngine;
using Unity.Netcode;
using System.Collections;

public class WaitingForPlayersManager : NetworkBehaviour
{
    public GameObject waitingPanel;

    private void Start()
    {
        if (IsServer)
        {
            NetworkManager.Singleton.OnClientConnectedCallback += CheckIfReadyToStart;
        }
    }

    private void OnDestroy()
    {
        if (IsServer)
        {
            NetworkManager.Singleton.OnClientConnectedCallback -= CheckIfReadyToStart;
        }
    }

    private void CheckIfReadyToStart(ulong clientId)
    {
        if (NetworkManager.Singleton.IsServer && NetworkManager.Singleton.ConnectedClientsList.Count == 2)
        {
            StartCoroutine(StartGame());
        }
    }

    private IEnumerator StartGame()
    {
        yield return new WaitForSeconds(1f); // Ensure all clients are fully connected
        HideWaitingPanelClientRpc();
    }

    [ClientRpc]
    private void HideWaitingPanelClientRpc()
    {
        if (waitingPanel != null)
        {
            waitingPanel.SetActive(false);
        }
    }
}
