using UnityEngine;
using UnityEngine.SceneManagement;
using Unity.Netcode;

public class PlayerMovement : NetworkBehaviour
{
    public float moveSpeed = 5f;
    private Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        if (rb == null)
        {
            Debug.LogError("Rigidbody component is missing.");
            return;
        }

        if (IsOwner)
        {
            DontDestroyOnLoad(gameObject);
            SceneManager.sceneLoaded += OnSceneLoaded;
            NetworkManager.Singleton.OnClientConnectedCallback += OnClientConnected;
        }
    }

    private void OnDestroy()
    {
        if (IsOwner)
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;
            NetworkManager.Singleton.OnClientConnectedCallback -= OnClientConnected;
        }
    }

    private void OnClientConnected(ulong clientId)
    {
        if (NetworkManager.Singleton.IsServer && NetworkManager.Singleton.ConnectedClientsList.Count == 2)
        {
            AssignCamera();
            MoveToSpawnPoint();
        }
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (NetworkManager.Singleton.IsServer && NetworkManager.Singleton.ConnectedClientsList.Count == 2)
        {
            AssignCamera();
            MoveToSpawnPoint();
        }
    }

    private void AssignCamera()
    {
        var mainCamera = Camera.main;
        if (mainCamera != null)
        {
            var cameraFollow = mainCamera.GetComponent<CameraFollow>();
            if (cameraFollow != null)
            {
                cameraFollow.SetTarget(transform);
            }
            else
            {
                Debug.LogError("CameraFollow component not found on the main camera.");
            }
        }
        else
        {
            Debug.LogError("Main Camera not found. Ensure there is a camera in the scene tagged as MainCamera.");
        }
    }

    private void MoveToSpawnPoint()
    {
        string spawnTag = IsHost ? "hostSpawn" : "clientSpawn";
        GameObject spawnPoint = GameObject.FindGameObjectWithTag(spawnTag);
        if (spawnPoint != null)
        {
            transform.position = spawnPoint.transform.position;
            transform.rotation = spawnPoint.transform.rotation;
        }
        else
        {
            Debug.LogError($"Spawn point with tag {spawnTag} not found!");
        }
    }

    void Update()
    {
        if (!IsOwner || rb == null) return;

        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        Vector3 direction = new Vector3(horizontal, 0f, vertical).normalized;

        if (direction.magnitude >= 0.1f)
        {
            Move(direction);
        }
    }

    void Move(Vector3 direction)
    {
        Vector3 moveDir = direction * moveSpeed * Time.deltaTime;
        rb.MovePosition(transform.position + moveDir);
    }
}
