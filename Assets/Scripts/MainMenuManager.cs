// MainMenuManager.cs
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
using Unity.Netcode;
using Unity.Netcode.Transports.UTP;
using System.Collections;

public class MainMenuManager : MonoBehaviour
{
    public TMP_InputField ipInputField;
    public TMP_InputField portInputField;
    public Button hostButton;
    public Button joinButton;
    public TextMeshProUGUI errorText; // Add a TextMeshProUGUI element for displaying errors

    public delegate void SceneLoadedHandler();
    public static event SceneLoadedHandler OnSceneLoadedEvent;

    void Start()
    {
        hostButton.onClick.AddListener(OnHostClicked);
        joinButton.onClick.AddListener(OnJoinClicked);
    }

    void OnHostClicked()
    {
        NetworkManager.Singleton.StartHost();
        StartCoroutine(LoadSceneAndNotify("Level1"));
    }

    void OnJoinClicked()
    {
        string ipAddress = ipInputField.text.Trim();
        string portString = portInputField.text.Trim();

        if (!ushort.TryParse(portString, out ushort port))
        {
            ShowError("Invalid port number. Please enter a valid port.");
            return;
        }

        var transport = NetworkManager.Singleton.GetComponent<UnityTransport>();
        transport.ConnectionData.Address = ipAddress;
        transport.ConnectionData.Port = port;

        NetworkManager.Singleton.StartClient();
        StartCoroutine(LoadSceneAndNotify("Level1"));
    }

    private IEnumerator LoadSceneAndNotify(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
        yield return new WaitUntil(() => SceneManager.GetActiveScene().name == sceneName);

        // Ensure everything is fully loaded
        yield return new WaitForSeconds(1f);

        OnSceneLoadedEvent?.Invoke();
    }

    private void ShowError(string message)
    {
        if (errorText != null)
        {
            errorText.text = message;
            errorText.gameObject.SetActive(true);
        }
    }
}
